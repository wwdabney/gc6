// Copyright © 2015 Steve Francia <spf@spf13.com>.
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.
//

package commands

import (
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"math/rand"
	"net/http"

	"github.com/golangchallenge/gc6/mazelib"
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
)

// Defining the icarus command.
// This will be called as 'laybrinth icarus'
var icarusCmd = &cobra.Command{
	Use:     "icarus",
	Aliases: []string{"client"},
	Short:   "Start the laybrinth solver",
	Long: `Icarus wakes up to find himself in the middle of a Labyrinth.
  Due to the darkness of the Labyrinth he can only see his immediate cell and if
  there is a wall or not to the top, right, bottom and left. He takes one step
  and then can discover if his new cell has walls on each of the four sides.

  Icarus can connect to a Daedalus and solve many laybrinths at a time.`,
	Run: func(cmd *cobra.Command, args []string) {
		RunIcarus()
	},
}

var ErrInvalid error = errors.New("invalid direction")

func init() {
	RootCmd.AddCommand(icarusCmd)
}

func RunIcarus() {
	// Run the solver as many times as the user desires.
	fmt.Println("Solving", viper.GetInt("times"), "times")
	for x := 0; x < viper.GetInt("times"); x++ {

		solveMaze()
	}

	// Once we have solved the maze the required times, tell daedalus we are done
	makeRequest("http://127.0.0.1:" + viper.GetString("port") + "/done")
}

// Make a call to the laybrinth server (daedalus) that icarus is ready to wake up
func awake() mazelib.Survey {
	contents, err := makeRequest("http://127.0.0.1:" + viper.GetString("port") + "/awake")
	if err != nil {
		fmt.Println(err)
	}
	r := ToReply(contents)
	return r.Survey
}

// Make a call to the laybrinth server (daedalus)
// to move Icarus a given direction
// Will be used heavily by solveMaze
func Move(direction string) (mazelib.Survey, error) {
	if direction == "left" || direction == "right" || direction == "up" || direction == "down" {

		contents, err := makeRequest("http://127.0.0.1:" + viper.GetString("port") + "/move/" + direction)
		if err != nil {
			return mazelib.Survey{}, err
		}

		rep := ToReply(contents)
		if rep.Victory == true {
			fmt.Println(rep.Message)
			// os.Exit(1)
			return rep.Survey, mazelib.ErrVictory
		} else {
			return rep.Survey, errors.New(rep.Message)
		}
	}

	return mazelib.Survey{}, ErrInvalid
}

// utility function to wrap making requests to the daedalus server
func makeRequest(url string) ([]byte, error) {
	response, err := http.Get(url)
	if err != nil {
		return nil, err
	}
	defer response.Body.Close()
	contents, err := ioutil.ReadAll(response.Body)
	if err != nil {
		return nil, err
	}
	return contents, nil
}

// Handling a JSON response and unmarshalling it into a reply struct
func ToReply(in []byte) mazelib.Reply {
	res := &mazelib.Reply{}
	json.Unmarshal(in, &res)
	return *res
}

// buildDirections returns a slice of directions that are not blocked by walls
func buildDirections(survey mazelib.Survey) []int {
	dirs := make([]int, 0)
	if !survey.Top {
		dirs = append(dirs, mazelib.N)
	}
	if !survey.Bottom {
		dirs = append(dirs, mazelib.S)
	}
	if !survey.Right {
		dirs = append(dirs, mazelib.E)
	}
	if !survey.Left {
		dirs = append(dirs, mazelib.W)
	}
	return dirs
}

// dirToString takes a direction mazelib.(N, S, E, W) and returns the
// corresponding direction command
func dirToString(dir int) string {
	switch dir {
	case mazelib.N:
		return "up"
	case mazelib.S:
		return "down"
	case mazelib.E:
		return "right"
	case mazelib.W:
		return "left"
	}
	return ""
}

// randomSolver attempts to solve the maze by making random moves
func randomSolver(survey mazelib.Survey) bool {
	var err error
	for {
		dirs := buildDirections(survey)
		index := rand.Intn(len(dirs))
		survey, err = Move(dirToString(dirs[index]))
		if err == mazelib.ErrVictory {
			return true
		}
	}
}

// backtrackSolver solves the maze by recursive backtracking.
// If the treasure is not found down this branch the function
// returns false and moves the agent back to where the parent began.
func backtrackSolver(survey mazelib.Survey, parent int) bool {
	var dir int
	reverse := reverseDir(parent)
	dirs := buildDirections(survey)

	// Try each possible direction in random order
	for _, i := range rand.Perm(len(dirs)) {
		dir = dirs[i]
		// Do not go back to parent location during exploration
		if dir == reverse {
			continue
		}
		// Convert direction to string and take move
		next, err := Move(dirToString(dir))

		// If treasure has been found return true, and does not attempt to backtrack
		if err == mazelib.ErrVictory {
			return true
		}

		// If we somehow sent an invalid error, we shouldn't follow the branch
		if err == ErrInvalid {
			continue
		}

		// No treasure found in one step, continue search along this branch
		if backtrackSolver(next, dir) {
			// Treasure found down this branch, return true without backtracking
			return true
		}
	}

	// No branches found treasure, backtrack to parent location and return false
	_, err := Move(dirToString(reverse))
	if err == ErrInvalid {
		panic("Unable to backtrack to previous location")
	}

	return false
}

// solveMaze wakes up in a new maze and attempts to solve it
func solveMaze() {
	survey := awake() // Need to start with waking up to initialize a new maze
	backtrackSolver(survey, -1)
}
